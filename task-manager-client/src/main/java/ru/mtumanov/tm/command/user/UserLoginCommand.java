package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "user login";
    }

    @Override
    @NotNull
    public String getName() {
        return "login";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRq request = new UserLoginRq(login, password);
        @NotNull final UserLoginRs response = getAuthEndpoint().login(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        setToken(response.getToken());
    }

}
