package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-cmd";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show command list";
    }

    @Override
    @NotNull
    public String getName() {
        return "commands";
    }

    @Override
    public void execute() {
        System.out.println("[Commands]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) continue;
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
