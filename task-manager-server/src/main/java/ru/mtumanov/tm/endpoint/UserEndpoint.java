package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.endpoint.IUserEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;
import ru.mtumanov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.mtumanov.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public UserChangePasswordRs userChangePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String id = session.getUserId();
            @Nullable final String password = request.getPassword();
            @NotNull final UserDTO user = getServiceLocator().getUserService().setPassword(id, password);
            return new UserChangePasswordRs(user);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserChangePasswordRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public UserLockRs userLock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRq request
    ) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            getServiceLocator().getUserService().lockUserByLogin(login);
            return new UserLockRs();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserLockRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public UserRegistryRs userRegistry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRq request
    ) {
        try {
            @Nullable final String login = request.getLogin();
            @Nullable final String password = request.getPassword();
            @Nullable final String email = request.getEmail();
            @NotNull final UserDTO user = getServiceLocator().getUserService().create(login, password, email);
            return new UserRegistryRs(user);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserRegistryRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public UserRemoveRs userRemove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRq request
    ) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            @NotNull final UserDTO user = getServiceLocator().getUserService().removeByLogin(login);
            return new UserRemoveRs(user);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserRemoveRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public UserUnlockRs userUnlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRq request
    ) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            getServiceLocator().getUserService().unlockUserByLogin(login);
            return new UserUnlockRs();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserUnlockRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public UserProfileRs userProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @NotNull final UserDTO user = getServiceLocator().getUserService().findById(userId);
            return new UserProfileRs(user);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserProfileRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public UserUpdateRs userUpdate(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @NotNull final String id = session.getUserId();
            @NotNull final String firstName = request.getFirstName();
            @NotNull final String lastName = request.getLastName();
            @NotNull final String middleName = request.getMiddleName();
            @NotNull final UserDTO user = getServiceLocator().getUserService().userUpdate(id, firstName, lastName, middleName);
            return new UserUpdateRs(user);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserUpdateRs(e);
        }
    }

}
