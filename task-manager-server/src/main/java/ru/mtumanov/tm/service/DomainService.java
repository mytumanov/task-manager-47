package ru.mtumanov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.IDomainService;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.data.DataLoadException;
import ru.mtumanov.tm.exception.data.DataSaveException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Base64;

public class DomainService implements IDomainService {

    @NotNull
    @Getter
    private IServiceLocator serviceLocator;

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private Domain getDomain() throws SQLException {
        @NotNull Domain domain = new Domain();
        domain.setProjects(getServiceLocator().getProjectService().findAll());
        domain.setTasks(getServiceLocator().getTaskService().findAll());
        domain.setUsers(getServiceLocator().getUserService().findAll());
        return domain;
    }

    private void setDomain(@Nullable final Domain domain) throws SQLException, AbstractException {
        if (domain == null)
            return;
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
    }

    @Override
    public void loadDataBase64() throws AbstractException, SQLException {
        try {
            @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
            @Nullable final String base64Data = new String(base64Byte);
            @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Data);
            @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            setDomain(domain);
        } catch (@NotNull IOException | @NotNull ClassNotFoundException e) {
            throw new DataLoadException();
        }
    }

    @Override
    public void saveDataBase64() throws AbstractException, SQLException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();

        try {
            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            byteArrayOutputStream.close();

            @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);

            fileOutputStream.write(base64.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (@NotNull IOException e) {
            throw new DataSaveException();
        }
    }

    @Override
    public void loadDataBinary() throws AbstractException, SQLException {
        try {
            @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            setDomain(domain);
        } catch (@NotNull IOException | @NotNull ClassNotFoundException e) {
            throw new DataLoadException();
        }
    }

    @Override
    public void saveDataBinary() throws AbstractException, SQLException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_BINARY);
            @NotNull final Path path = file.toPath();
            Files.deleteIfExists(path);
            Files.createFile(path);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (@NotNull IOException e) {
            throw new DataSaveException();
        }
    }

    @Override
    public void loadDataJsonFasterXml() throws AbstractException, SQLException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
            @NotNull final String json = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
            setDomain(domain);
        } catch (IOException e) {
            throw new DataLoadException();
        }
    }

    @Override
    public void saveDataJsonFasterXml() throws AbstractException, SQLException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new DataSaveException();
        }
    }

    @Override
    public void loadDataJsonJaxb() throws AbstractException, SQLException {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        try {
            @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (JAXBException e) {
            throw new DataLoadException();
        }
    }

    @Override
    public void saveDataJsonJaxb() throws AbstractException, SQLException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException | JAXBException e) {
            throw new DataSaveException();
        }
    }

    @Override
    public void loadDataXmlFasterXml() throws AbstractException, SQLException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
            @NotNull final String yaml = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
            setDomain(domain);
        } catch (IOException e) {
            throw new DataLoadException();
        }
    }

    @Override
    public void saveDataXmlFasterXml() throws AbstractException, SQLException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new DataSaveException();
        }
    }

    @Override
    public void loadDataXmlJaxb() throws AbstractException, SQLException {
        try {
            @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            @NotNull final File file = new File(FILE_XML);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (JAXBException e) {
            throw new DataLoadException();
        }
    }

    @Override
    public void saveDataXmlJaxb() throws AbstractException, SQLException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException | JAXBException e) {
            throw new DataSaveException();
        }
    }

    @Override
    public void loadDataYamlFasterXml() throws AbstractException, SQLException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
            @NotNull final String yaml = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
            setDomain(domain);
        } catch (IOException e) {
            throw new DataLoadException();
        }
    }

    @Override
    public void saveDataYamlFasterXml() throws AbstractException, SQLException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_YAML);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(yaml.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new DataSaveException();
        }
    }

}
