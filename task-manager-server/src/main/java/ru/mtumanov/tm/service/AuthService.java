package ru.mtumanov.tm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.IAuthService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.dto.IDtoSessionService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.system.FatalSystemException;
import ru.mtumanov.tm.exception.user.AccessDeniedException;
import ru.mtumanov.tm.exception.user.AuthenticationException;
import ru.mtumanov.tm.exception.user.UserNotFoundException;
import ru.mtumanov.tm.util.CryptUtil;
import ru.mtumanov.tm.util.HashUtil;

import java.util.Date;

public class AuthService implements IAuthService {

    @NotNull
    private final IDtoUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IDtoSessionService sessionService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IDtoUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IDtoSessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @Override
    @NotNull
    public String login(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty())
            throw new LoginEmptyException();
        if (password == null || password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final UserDTO user;
        try {
            user = userService.findByLogin(login);
        } catch (UserNotFoundException e) {
            throw new AccessDeniedException();
        }
        if (user.getPasswordHash() == null)
            throw new AccessDeniedException();
        if (user.isLocked())
            throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!user.getPasswordHash().equals(hash))
            throw new AuthenticationException();
        userId = user.getId();
        @NotNull final String token;
        try {
            token = getToken(user);
        } catch (@NotNull final Exception e) {
            throw new FatalSystemException(e);
        }
        return token;
    }

    @Override
    @NotNull
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return userService.create(login, password, email);
    }

    @Override
    @NotNull
    public SessionDTO validateToken(@Nullable final String token) throws AbstractException {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String json;
        @NotNull final SessionDTO session;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            session = objectMapper.readValue(json, SessionDTO.class);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getCreated();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        return session;
    }

    @NotNull
    private String getToken(@NotNull final SessionDTO session) throws JsonProcessingException {
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private String getToken(@NotNull final UserDTO user) throws JsonProcessingException, AbstractException {
        return getToken(createSession(user));
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) throws AbstractException {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        sessionService.add(session.getUserId(), session);
        return session;
    }

    @Override
    public void logout(@Nullable final SessionDTO session) throws AbstractException {
        if (session == null) return;
        sessionService.remove(session.getUserId(), session);
    }
}
