package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.dto.IDtoProjectService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.field.StatusNotSupportedException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;

public class ProjectDtoService extends AbstractDtoUserOwnedService<ProjectDTO, IDtoProjectRepository>
        implements IDtoProjectService {

    public ProjectDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected IDtoProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    @Override
    @NotNull
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);

        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @NotNull
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ProjectDTO project = getRepository(entityManager).findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);

        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @NotNull
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (status == null)
            throw new StatusNotSupportedException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ProjectDTO project = getRepository(entityManager).findOneById(userId, id);
        project.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
