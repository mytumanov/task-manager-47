package ru.mtumanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Comparator;
import java.util.List;

public class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements IDtoTaskRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    public TaskDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM TaskDTO WHERE userId = :userId")
                .setParameter(USER_ID, userId)
                .executeUpdate();

    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("FROM TaskDTO p WHERE p.userId = :userId", TaskDTO.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(@NotNull final String userId, @NotNull final Comparator<TaskDTO> comparator) {
        return entityManager
                .createQuery("FROM TaskDTO p WHERE p.userId = :userId ORDER BY p." + getComporator(comparator),
                        TaskDTO.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public TaskDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("FROM TaskDTO p WHERE p.userId = :userId AND p.id = :id", TaskDTO.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(1) FROM TaskDTO p WHERE p.userId = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM TaskDTO p WHERE p.userId = :userId AND p.id = :id")
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .executeUpdate();

    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        try {
            findOneById(userId, id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO")
                .executeUpdate();

    }

    @Override
    @NotNull
    public List<TaskDTO> findAll() {
        return entityManager.createQuery("FROM TaskDTO p", TaskDTO.class)
                .getResultList();
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(@NotNull final Comparator<TaskDTO> comparator) {
        return entityManager.createQuery("FROM TaskDTO p ORDER BY p." + getComporator(comparator), TaskDTO.class)
                .getResultList();
    }

    @Override
    @Nullable
    public TaskDTO findOneById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM TaskDTO p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM TaskDTO WHERE id = :id")
                .setParameter(ID, id)
                .executeUpdate();

    }

    @Override
    public boolean existById(@NotNull final String id) {
        try {
            findOneById(id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    @NotNull
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager
                .createQuery("FROM TaskDTO t WHERE userId = :userId AND projectId = :projectId", TaskDTO.class)
                .setParameter(USER_ID, userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTaskByProjectId(@NotNull final String projectId) {
        entityManager.createQuery("DELETE FROM TaskDTO WHERE projectId = :projectId")
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
