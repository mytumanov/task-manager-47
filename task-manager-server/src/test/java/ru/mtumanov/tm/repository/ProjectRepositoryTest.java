package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.service.ConnectionService;
import ru.mtumanov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<ProjectDTO> projectList = new ArrayList<>();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IDtoProjectRepository projectRepository = new ProjectDtoRepository(connectionService.getEntityManager());

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("ProjectDTO name: " + i);
            project.setDescription("ProjectDTO description: " + i);
            if (i <= 5)
                project.setUserId(USER_ID_1);
            else
                project.setUserId(USER_ID_2);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @After
    public void clearRepository() throws Exception {
        projectRepository.clear();
        projectList.clear();
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test project name");
        project.setDescription("Test project description");
        project.setUserId(USER_ID_1);

        projectList.add(project);
        projectRepository.add(project);
        @NotNull final List<ProjectDTO> actualProjectList = projectRepository.findAll();
        assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testClear() throws Exception {
        assertNotEquals(0, projectRepository.getSize(USER_ID_1));
        projectRepository.clear();
        assertEquals(0, projectRepository.getSize(USER_ID_1));
    }

    @Test
    public void testExistById() throws Exception {
        for (@NotNull final ProjectDTO project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectRepository.existById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<ProjectDTO> projectsUser1 = new ArrayList<>();
        @NotNull final List<ProjectDTO> projectsUser2 = new ArrayList<>();
        for (@NotNull final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }
        assertNotEquals(projectsUser1, projectsUser2);

        @NotNull final List<ProjectDTO> actualProjectsUser1 = projectRepository.findAll(USER_ID_1);
        @NotNull final List<ProjectDTO> actualProjectsUser2 = projectRepository.findAll(USER_ID_2);
        assertEquals(projectsUser1, actualProjectsUser1);
        assertEquals(projectsUser2, actualProjectsUser2);
    }

    @Test
    public void testFindOneById() throws Exception {
        for (@NotNull final ProjectDTO project : projectList) {
            assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionFindOneById() throws Exception {
        for (int i = 0; i < 10; i++) {
            projectRepository.findOneById(USER_ID_1, UUID.randomUUID().toString());
        }
    }

    @Test
    public void testGetSize() throws Exception {
        assertEquals(projectList.size(), projectRepository.getSize(USER_ID_1));
    }

    @Test
    public void testRemove() throws Exception {
        for (@NotNull final ProjectDTO project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
            projectRepository.removeById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemove() throws Exception {
        for (int i = 0; i < 10; i++) {
            projectRepository.removeById(USER_ID_1, "123");
        }
    }

    @Test
    public void testRemoveById() throws Exception {
        for (@NotNull final ProjectDTO project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
            projectRepository.removeById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemoveById() throws Exception {
        for (int i = 0; i < 10; i++) {
            projectRepository.removeById(USER_ID_2, UUID.randomUUID().toString());
        }
    }

}
