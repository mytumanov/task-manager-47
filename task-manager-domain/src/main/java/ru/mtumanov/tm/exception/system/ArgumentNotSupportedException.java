package ru.mtumanov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSytemException {

    public ArgumentNotSupportedException() {
        super("ERROR! Argument not supported!");
    }

    public ArgumentNotSupportedException(@NotNull final String arg) {
        super("ERROR! Argument: " + arg + " not supported!");
    }

}
