package ru.mtumanov.tm.exception.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractDataException extends AbstractException {

    protected AbstractDataException() {
    }

    protected AbstractDataException(@NotNull final String message) {
        super(message);
    }

    protected AbstractDataException(@NotNull final String messgae, @NotNull final Throwable cause) {
        super(messgae, cause);
    }

    protected AbstractDataException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractDataException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
