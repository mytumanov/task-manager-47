package ru.mtumanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.model.IWBS;
import ru.mtumanov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(nullable = false)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    public TaskDTO(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ID:" + id + " " +
                "NAME:" + name + " " +
                "DESCRIPTION:" + description + " " +
                "PROJECTID:" + projectId + " " +
                "STATUS:" + getStatus().getDisplayName() + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TaskDTO)) {
            return false;
        }
        TaskDTO task = (TaskDTO) o;
        return Objects.equals(name, task.name)
                && Objects.equals(description, task.description)
                && Objects.equals(status, task.status)
                && Objects.equals(projectId, task.projectId)
                && Objects.equals(created, task.created)
                && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, projectId, created);
    }

}
