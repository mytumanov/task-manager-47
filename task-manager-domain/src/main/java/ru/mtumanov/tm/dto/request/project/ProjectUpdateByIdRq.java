package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIdRq(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(token);
        this.id = id;
        this.name = name;
        this.description = description;
    }

}