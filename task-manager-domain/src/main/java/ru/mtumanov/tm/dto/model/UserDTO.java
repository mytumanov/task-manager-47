package ru.mtumanov.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.Role;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    @NotNull
    @Column(nullable = false)
    private String login;

    @NotNull
    @Column(nullable = false, name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "mid_name")
    private String middleName;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(nullable = false)
    private boolean locked = false;

}
