package ru.mtumanov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AbstractUserOwnedModel)) {
            return false;
        }
        AbstractUserOwnedModel abstractUserOwnedModel = (AbstractUserOwnedModel) o;
        return Objects.equals(user.getId(), abstractUserOwnedModel.user.id)
                && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(user.id);
    }

}
